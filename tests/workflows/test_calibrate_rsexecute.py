"""Unit tests for calibration graphs


"""
import logging
import os
import sys
import unittest

import numpy
from astropy import units as u
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.calibration import create_gaintable_from_visibility
from ska_sdp_datamodels.configuration.config_create import create_named_configuration
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame
from ska_sdp_func_python.calibration import apply_gaintable, create_calibration_controls

from rascil.processing_components.simulation import ingest_unittest_visibility
from rascil.processing_components.simulation import simulate_gaintable
from rascil.workflows.rsexecute.calibration.calibration_rsexecute import (
    calibrate_list_rsexecute_workflow,
)
from rascil.workflows.rsexecute.execution_support.rsexecute import rsexecute

log = logging.getLogger("rascil-logger")

log.setLevel(logging.WARNING)
log.addHandler(logging.StreamHandler(sys.stdout))

log.setLevel(logging.WARNING)


class TestCalibrateGraphs(unittest.TestCase):
    def setUp(self):
        rsexecute.set_client(use_dask=True)

        from rascil.processing_components.parameters import (
            rascil_path,
        )

        self.results_dir = rascil_path("test_results")

        self.persist = os.getenv("RASCIL_PERSIST", False)

    def tearDown(self):
        rsexecute.close()

    def actualSetUp(
        self,
        nfreqwin=3,
        dospectral=True,
        dopol=False,
        amp_errors=None,
        phase_errors=None,
        zerow=True,
    ):
        if amp_errors is None:
            amp_errors = {"T": 0.0, "G": 0.1}
        if phase_errors is None:
            phase_errors = {"T": 1.0, "G": 0.0}

        self.npixel = 512
        self.low = create_named_configuration("LOWBD2", rmax=750.0)
        self.freqwin = nfreqwin
        self.vis_list = list()
        self.ntimes = 1
        self.times = numpy.linspace(-3.0, +3.0, self.ntimes) * numpy.pi / 12.0
        self.frequency = numpy.linspace(0.8e8, 1.2e8, self.freqwin)

        if self.freqwin > 1:
            self.channelwidth = numpy.array(
                self.freqwin * [self.frequency[1] - self.frequency[0]]
            )
        else:
            self.channelwidth = numpy.array([1e6])

        if dopol:
            self.vis_pol = PolarisationFrame("linear")
            self.image_pol = PolarisationFrame("stokesIQUV")
            f = numpy.array([100.0, 20.0, -10.0, 1.0])
        else:
            self.vis_pol = PolarisationFrame("stokesI")
            self.image_pol = PolarisationFrame("stokesI")
            f = numpy.array([100.0])

        if dospectral:
            flux = numpy.array(
                [f * numpy.power(freq / 1e8, -0.7) for freq in self.frequency]
            )
        else:
            flux = numpy.array([f])

        self.phasecentre = SkyCoord(
            ra=+180.0 * u.deg, dec=-60.0 * u.deg, frame="icrs", equinox="J2000"
        )
        self.vis_list = [
            rsexecute.execute(ingest_unittest_visibility, nout=1)(
                self.low,
                [self.frequency[i]],
                [self.channelwidth[i]],
                self.times,
                self.vis_pol,
                self.phasecentre,
                zerow=zerow,
            )
            for i in range(nfreqwin)
        ]
        self.vis_list = rsexecute.compute(self.vis_list, sync=True)

        for v in self.vis_list:
            v["vis"].data[...] = 1.0 + 0.0j

        self.error_vis_list = [
            rsexecute.execute(v.copy)(deep=True) for v in self.vis_list
        ]
        gt = rsexecute.execute(create_gaintable_from_visibility)(
            self.vis_list[0], jones_type="G"
        )
        gt = rsexecute.execute(simulate_gaintable)(
            gt,
            phase_error=0.1,
            amplitude_error=0.0,
            smooth_channels=1,
            leakage=0.0,
            seed=180555,
        )
        self.error_vis_list = [
            rsexecute.execute(apply_gaintable)(
                self.error_vis_list[i], gt, use_flags=False
            )
            for i in range(self.freqwin)
        ]

        self.error_vis_list = rsexecute.compute(self.error_vis_list, sync=True)

        assert (
            numpy.max(numpy.abs(self.error_vis_list[0].vis - self.vis_list[0].vis))
            > 0.0
        )

    def test_calibrate_rsexecute(self):
        amp_errors = {"T": 0.0, "G": 0.0}
        phase_errors = {"T": 1.0, "G": 0.0}
        self.actualSetUp(amp_errors=amp_errors, phase_errors=phase_errors)

        controls = create_calibration_controls()
        controls["T"]["first_selfcal"] = 0
        controls["T"]["timeslice"] = "auto"

        calibrate_list = calibrate_list_rsexecute_workflow(
            self.error_vis_list,
            self.vis_list,
            calibration_context="T",
            controls=controls,
            do_selfcal=True,
            global_solution=False,
        )
        calibrate_list = rsexecute.compute(calibrate_list, sync=True)

        assert len(calibrate_list) == 2
        assert numpy.max(calibrate_list[1][0]["T"].residual) < 7e-6, numpy.max(
            calibrate_list[1][0]["T"].residual
        )
        err = numpy.max(
            numpy.abs(
                calibrate_list[0][0].visibility_acc.flagged_vis
                - self.vis_list[0].visibility_acc.flagged_vis
            )
        )
        assert err < 2e-6, err

    def test_calibrate_rsexecute_repeat(self):
        amp_errors = {"T": 0.0, "G": 0.0}
        phase_errors = {"T": 1.0, "G": 0.0}
        self.actualSetUp(amp_errors=amp_errors, phase_errors=phase_errors)

        controls = create_calibration_controls()
        controls["T"]["first_selfcal"] = 0
        controls["T"]["timeslice"] = "auto"

        calibrate_list = calibrate_list_rsexecute_workflow(
            self.error_vis_list,
            self.vis_list,
            calibration_context="T",
            controls=controls,
            do_selfcal=True,
            global_solution=False,
        )
        calibrate_list = rsexecute.compute(calibrate_list, sync=True)

        assert len(calibrate_list) == 2
        assert numpy.max(calibrate_list[1][0]["T"].residual) < 7e-6, numpy.max(
            calibrate_list[1][0]["T"].residual
        )
        err = numpy.max(
            numpy.abs(
                calibrate_list[0][0].visibility_acc.flagged_vis
                - self.vis_list[0].visibility_acc.flagged_vis
            )
        )
        assert err < 2e-6, err

        calibrate_list = calibrate_list_rsexecute_workflow(
            self.error_vis_list,
            self.vis_list,
            gt_list=calibrate_list[1],
            calibration_context="T",
            controls=controls,
            do_selfcal=True,
            global_solution=False,
        )
        calibrate_list = rsexecute.compute(calibrate_list, sync=True)

        assert len(calibrate_list) == 2
        assert numpy.max(calibrate_list[1][0]["T"].residual) < 7e-6, numpy.max(
            calibrate_list[1][0]["T"].residual
        )
        err = numpy.max(
            numpy.abs(
                calibrate_list[0][0].visibility_acc.flagged_vis
                - self.vis_list[0].visibility_acc.flagged_vis
            )
        )
        assert err < 2e-6, err

    def test_calibrate_rsexecute_empty(self):
        amp_errors = {"T": 0.0, "G": 0.0}
        phase_errors = {"T": 1.0, "G": 0.0}
        self.actualSetUp(amp_errors=amp_errors, phase_errors=phase_errors)

        for v in self.vis_list:
            v["vis"].data[...] = 0.0 + 0.0j

        controls = create_calibration_controls()
        controls["T"]["first_selfcal"] = 0
        controls["T"]["timeslice"] = "auto"

        calibrate_list = calibrate_list_rsexecute_workflow(
            self.error_vis_list,
            self.vis_list,
            calibration_context="T",
            controls=controls,
            do_selfcal=True,
            global_solution=False,
        )
        calibrate_list = rsexecute.compute(calibrate_list, sync=True)
        assert len(calibrate_list[1][0]) > 0

    def test_calibrate_rsexecute_global(self):
        amp_errors = {"T": 0.0, "G": 0.0}
        phase_errors = {"T": 1.0, "G": 0.0}
        self.actualSetUp(amp_errors=amp_errors, phase_errors=phase_errors)

        controls = create_calibration_controls()
        controls["T"]["first_selfcal"] = 0
        controls["T"]["timeslice"] = "auto"

        calibrate_list = calibrate_list_rsexecute_workflow(
            self.error_vis_list,
            self.vis_list,
            calibration_context="T",
            controls=controls,
            do_selfcal=True,
            global_solution=True,
        )

        calibrate_list = rsexecute.compute(calibrate_list, sync=True)

        assert len(calibrate_list) == 2
        assert numpy.max(calibrate_list[1][0]["T"].residual) < 7e-6, numpy.max(
            calibrate_list[1][0]["T"].residual
        )
        err = numpy.max(
            numpy.abs(
                calibrate_list[0][0].visibility_acc.flagged_vis
                - self.vis_list[0].visibility_acc.flagged_vis
            )
        )
        assert err < 2e-6, err

    def test_calibrate_rsexecute_global_empty(self):
        amp_errors = {"T": 0.0, "G": 0.0}
        phase_errors = {"T": 1.0, "G": 0.0}
        self.actualSetUp(amp_errors=amp_errors, phase_errors=phase_errors)

        for v in self.vis_list:
            v["vis"].data[...] = 0.0 + 0.0j

        controls = create_calibration_controls()
        controls["T"]["first_selfcal"] = 0
        controls["T"]["timeslice"] = "auto"

        calibrate_list = calibrate_list_rsexecute_workflow(
            self.error_vis_list,
            self.vis_list,
            calibration_context="T",
            controls=controls,
            do_selfcal=True,
            global_solution=True,
        )

        calibrate_list = rsexecute.compute(calibrate_list, sync=True)
        assert len(calibrate_list[1][0]) > 0


if __name__ == "__main__":
    unittest.main()
