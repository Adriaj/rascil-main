""" Unit tests for visibility operations


"""

import unittest

import astropy.units as u
import numpy
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.configuration.config_create import create_named_configuration
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame
from ska_sdp_datamodels.sky_model.sky_model import SkyComponent
from ska_sdp_datamodels.visibility import create_visibility

from rascil.processing_components.flagging.operations import (
    flagging_visibility,
    flagging_aoflagger,
)


class TestFlaggingOperations(unittest.TestCase):
    def setUp(self):
        self.lowcore = create_named_configuration("LOWBD2-CORE")
        self.times = (numpy.pi / 43200.0) * numpy.arange(0.0, 300.0, 30.0)
        self.frequency = numpy.linspace(1.0e8, 1.1e8, 3)
        self.channel_bandwidth = numpy.array([1e7, 1e7, 1e7])
        # Define the component and give it some spectral behaviour
        f = numpy.array([100.0, 20.0, -10.0, 1.0])
        self.flux = numpy.array([f, 0.8 * f, 0.6 * f])
        self.polarisation_frame = PolarisationFrame("linear")

        # The phase centre is absolute and the component is specified relative (for now).
        # This means that the component should end up at the position phasecentre+compredirection
        self.phasecentre = SkyCoord(
            ra=+180.0 * u.deg, dec=-35.0 * u.deg, frame="icrs", equinox="J2000"
        )
        self.compabsdirection = SkyCoord(
            ra=+181.0 * u.deg, dec=-35.0 * u.deg, frame="icrs", equinox="J2000"
        )
        pcof = self.phasecentre.skyoffset_frame()
        self.compreldirection = self.compabsdirection.transform_to(pcof)
        self.comp = SkyComponent(
            direction=self.compreldirection, frequency=self.frequency, flux=self.flux
        )

    def test_flagging_visibility_multiple(self):
        bvis = create_visibility(
            self.lowcore,
            self.times,
            self.frequency,
            channel_bandwidth=self.channel_bandwidth,
            phasecentre=self.phasecentre,
            polarisation_frame=self.polarisation_frame,
            weight=1.0,
        )
        baselines = [100, 199]
        antennas = [1, 3]
        channels = [0, 1]
        pols = [0]
        bvis = flagging_visibility(
            bvis,
            baselines=baselines,
            antennas=antennas,
            channels=channels,
            polarisations=pols,
        )
        # Check flagging on baselines
        for baseline in baselines:
            assert bvis["flags"].data[:, baseline, ...].all() == 1
        # Check flagging on channels
        for channel in channels:
            assert bvis["flags"].data[..., channel, :].all() == 1
        # Check flagging on pols
        for pol in pols:
            assert (bvis["flags"].data[..., pol] == 1).all()
        # Check flagging on antennas
        for ibaseline, (a1, a2) in enumerate(bvis.baselines.data):
            if a1 in antennas or a2 in antennas:
                assert (bvis["flags"].data[:, ibaseline, ...] == 1).all()

    def test_flagging_visibility_rfi(self):
        bvis = create_visibility(
            self.lowcore,
            self.times,
            self.frequency,
            channel_bandwidth=self.channel_bandwidth,
            phasecentre=self.phasecentre,
            polarisation_frame=self.polarisation_frame,
            weight=1.0,
        )
        rfi_start = int(len(self.times) * 0.2)
        rfi_end = int(len(self.times) * 0.6)
        rfi_ch = 1

        rfi_mask = numpy.zeros_like(bvis["vis"].data, dtype=bool)
        rfi_mask[rfi_start:rfi_end, :, rfi_ch, :] = True
        bvis["vis"].data[rfi_mask] = +10

        bvis = flagging_aoflagger(
            bvis,
            "generic",
        )

        # Check flagging on rfi
        assert bvis["flags"].data[rfi_mask].all() == 1
        assert bvis["flags"].data[~rfi_mask].all() == 0


if __name__ == "__main__":
    unittest.main()
